//Gruntfile
module.exports = function(grunt) {

    // Optimizing build times
    require('time-grunt')(grunt);

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Global Variables
    var sitePath = {
        dest: './public/assets/site',
        src: './resources/assets/site',
        vendor: './resources/assets/vendor'
    };

    // Initializing the configuration object
    grunt.initConfig({

        // Vars
        sitePath: sitePath,

        // JSHint
        jshint: {
            files: [
                '<%= sitePath.src %>/js/**/*.js'
            ],
            options: {
                globals: {
                    jQuery: true
                }
            }
        },

        // Concat
        concat: {
            options: {
                separator: ';'
            },
            js_site: {
                src: [

                    // Vendor
                    '<%= sitePath.vendor %>/jquery/jquery-2.2.4.js',
                    '<%= sitePath.vendor %>/jquery/jquery.mobile.custom.js', 
                    '<%= sitePath.vendor %>/breakpoints/breakpoints.js',

                    // Sources
                    '<%= sitePath.src %>/js/main.js'
                ],
                dest: '<%= sitePath.dest %>/js/main.js'
            }
        },

        // Uglify
        uglify: {
            options: {
                mangle: false // Use if you want the names of your functions and variables unchanged
            },
            site: {
                files: {
                    '<%= sitePath.dest %>/js/main.min.js' : '<%= sitePath.dest %>/js/main.js'
                }
            }
        },

        // SASS
        sass: {
            dist: {
                files: {
                    '<%= sitePath.dest %>/css/main.css' : '<%= sitePath.src %>/sass/main.scss'
                }
            }
        },

        // CSS minify
        cssmin: {
            options: {
                shorthandCompaction: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    '<%= sitePath.dest %>/css/main.min.css': '<%= sitePath.dest %>/css/main.css'
                }
            }
        },

        // sprite: {
        //     clients: {
        //         src: '<%= sitePath.src %>/images/*.png',
        //         retinaSrcFilter: '<%= sitePath.src %>/images/*@2x.png',
        //         dest: '<%= sitePath.dest %>/images/sprite@1x.png',
        //         retinaDest: '<%= sitePath.dest %>/images/sprite@2x.png',
        //         imgPath: '../images/sprite@1x.png',
        //         retinaImgPath: '../images/sprite@2x.png',
        //         algorithm: 'top-down',
        //         padding: 2,
        //         destCss: '<%= sitePath.src %>/less/helpers/_clients-logo.less',
        //         format: 'less'
        //     }
        // },

        // Watch
        watch: {
            js: {
                files: [
                    './Gruntfile.js',
                    '<%= sitePath.src %>/js/**/*.js'
                ],
                tasks: ['concat:js_site'],
                options: { livereload: true }
            },
            sass: {
                files: [
                    '<%= sitePath.src %>/sass/**/*.scss'
                ],
                tasks: ['sass'],
                options: { livereload: true }
            }
        },

        // Copy files to public
        copy: {
            main: {
                files: [
                    // { expand: true, cwd: './vendor/', src: 'zurb/**', dest: '<%= sitePath.vendor %>' },
                    { expand: true, cwd: '<%= sitePath.src %>', src: ['images/**'], dest: '<%= sitePath.dest %>' }
                ]
            }
        },

        // Clean folders for public directory
        clean: {
            build: {
                src: ['<%= sitePath.dest %>']
            }
        },

        // Assets versioning
        ver: {
            site_css: {
                phases: [
                    {
                        files: [
                            '<%= sitePath.dest %>/css/main.min.css',
                        ]
                    },
                ],
                versionFile: '<%= sitePath.dest %>/css/version.json'
            },
            site_js: {
                phases: [
                    {
                        files: [
                            '<%= sitePath.dest %>/js/main.min.js',
                        ]
                    },
                ],
                versionFile: '<%= sitePath.dest %>/js/version.json'
            }
        }
    });

    // Task definition
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('dev', ['clean', 'copy', 'concat', 'sass', 'watch']);
    grunt.registerTask('build', ['clean', 'copy', 'concat', 'uglify', 'sass', 'cssmin', 'ver']);
};