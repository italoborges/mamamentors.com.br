<?php

/*
|--------------------------------------------------------------------------
| Site routes
|--------------------------------------------------------------------------
*/

// Home
Route::get('/', ['as' => 'site.home.index', 'uses' => 'Site\HomeController@index']);

// Thank you
Route::get('/thank-you', ['as' => 'site.thankyou.index', 'uses' => 'Site\ThankyouController@index']);

// Terms Of Use
Route::get('/terms-of-use', ['as' => 'site.termsofuse.index', 'uses' => 'Site\TermsofuseController@index']);

// About us
Route::get('/about-us', ['as' => 'site.about.index', 'uses' => 'Site\AboutController@index']);