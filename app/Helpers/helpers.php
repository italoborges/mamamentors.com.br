<?php

/**
 * Get the path to a versioned file.
 *
 * @param  string  $file
 * @return string
 */
function asset_version($file)
{
    $filename = basename($file);
    $path = '/' . ltrim(dirname($file), '/');
    $versionFile = json_decode(file_get_contents(public_path().$path.'/version.json'), true);

    if (isset($versionFile[$filename])) {
        return asset($path.'/'.$versionFile[$filename]);
    }

    throw new InvalidArgumentException("File {$filename} not defined in the version file.");
}