@extends('site.layout.default')

@section('title', 'Thank You')
@section('description', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.')
@section('id', 'thankyou')
@section('content')

    <section class="thankyou">
        <div class="column row">
            <div class="small-12 columns">
                <h2 class="section-title text-center space-out-bottom-small">Thank you for supporting us!</h2>
                <p class="text-paragraph text-center space-out-bottom-default">Help <span class="text-bold text-color-pink">spread the word</span> and grow the Mama Mentors community by inviting friends to join!</p>
            </div>
        </div>
        <div class="column row">
            <div class="small-12 columns">
                <ul class="list list-invite">
                    <li class="invite-item space-out-bottom-small">
                        <a href="#" class="button-invite facebook float-center">
                            Invite on facebook
                            <span class="icon float-right">
                                <?php echo file_get_contents("assets/site/images/icon-facebook.svg"); ?>
                            </span>
                        </a>
                    </li>
                    <li class="invite-item space-out-bottom-small">
                        <a href="#" class="button-invite twitter float-center">
                            Invite on twitter
                            <span class="icon  float-right">
                                <?php echo file_get_contents("assets/site/images/icon-twitter.svg"); ?>
                            </span>
                        </a>
                    </li>
                    <li class="invite-item space-out-bottom-small">
                        <a href="#" class="button-invite email float-center">
                            Invite by e-mail
                            <span class="icon float-right">
                                <?php echo file_get_contents("assets/site/images/icon-mail.svg"); ?>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
	
@endsection