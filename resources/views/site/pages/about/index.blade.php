@extends('site.layout.default')

@section('title', 'About Us')
@section('description', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.')
@section('id', 'aboutus')
@section('content')

    <section class="founder">
        <div class="column row">

        </div>
    </section>

    <section class="the-team">
        
    </section>

    <section class="timeline">
        
    </section>

    <section class="support space-in-bigger">
		<div class="column row">
			<div class="small-12 columns">
				<img src="assets/site/images/icon-private-message-white.png" alt="Icon private message"
					width="41" height="auto" class="float-center">
				<h2 class="section-title text-color-white text-center space-out-top-small space-out-bottom-big">I <span class="text-font-bold">support</span> Mama Mentors because</h2>
			</div>
		</div>
		<div class="row expanded">
			<div class="list-testmonial">
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-1@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-1@2x.jpg, assets/site/images/testmonial-1@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								Being a mom is the hardest, most rewarding thing I've ever done. There are daily challenges and rewards that only another mama can truly appreciate. Having someone to share frustrations and victories with (no matter how small) and turn to for advice is so important.
								<span class="author">
									Ashley F., Portland, OR
								</span>
							</p>
						</div>
					</div>
				</a>
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-2@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-2@2x.jpg, assets/site/images/testmonial-2@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								It takes a village...sometimes you need that village to give you advice and direction and sometimes you just need the village to say "you are doing the best you can and there is nothing more important than that!" And maybe bring you a glass of wine from time to time.
								<span class="author">
									Whitney T., Boston, Massachusetts
								</span>
							</p>
						</div>
					</div>
				</a>
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-1@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-1@2x.jpg, assets/site/images/testmonial-1@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								Being a mom is the hardest, most rewarding thing I've ever done. There are daily challenges and rewards that only another mama can truly appreciate. Having someone to share frustrations and victories with (no matter how small) and turn to for advice is so important.
								<span class="author">
									Ashley F., Portland, OR
								</span>
							</p>
						</div>
					</div>
				</a>
			</ul>
		</div>
		<div class="column row show-for-small-only">
			<ul class="list testmonials-pagination">
				<li class="page-item">
					<a href="#" class="page-link">Page</a>
				</li>
				<li class="page-item">
					<a href="#" class="page-link active">Page</a>
				</li>
				<li class="page-item">
					<a href="#" class="page-link">Page</a>
				</li>
			</ul>
		</div>
	</section>

    <section class="quote space-in-bigger">
		<div class="column row">
			<div class="small-12 columns">
				<div class="brand logo-vertical float-center">
					<?php echo file_get_contents("assets/site/images/logo-mamamentors-vertical.svg"); ?>
				</div>
			</div>
			<div class="small-12 columns">
				<h2 class="section-title text-color-grey text-center"><span class="text-font-bold text-color-pink">Share</span> the drama with your Mama.</h2>
			</div>
		</div>
	</section>

	<section class="subscribe space-in-big diagonal-top diagonal-bottom">
		<div class="column row">
			<div class="small-12 columns">
				<h3 class="help-text text-center text-font-black text-color-blue text-uppercase space-out-bottom-smaller">Get Involved</h3>
				<h2 class="section-title text-center space-out-bottom-small">Subscribe to Our Newsletter</h2>
				<p class="text-paragraph text-center space-out-bottom-default">
					Sign up for updates and receive an early invitation when we launch our beta.  As mentor, mentee or both – we can empower every woman to experience motherhood as a team.
				</p>
			</div>
		</div>
		<div class="column row">
			<form action="#">
				<div class="small-12 medium-6 large-offset-1 large-5 columns">
					<div class="form-group">
						<input class="input-field space-out-bottom-small" type="text" placeholder="enter email address">	
					</div>
				</div>
				<div class="small-12 medium-6 large-pull-1 large-5 columns">
					<div class="form-group">
						<button class="button-solid" type="submit">sign up and get updates</button>	
					</div>
				</div>	
			</form>
		</div>
	</section>
	
@endsection