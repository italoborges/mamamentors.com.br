@extends('site.layout.default')

@section('title', 'Terms of Use')
@section('description', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.')
@section('id', 'termsofuse')
@section('content')

    <section class="terms">
        <div class="column row">
            <div class="small-12 columns">
                <h2 class="section-title space-out-bottom-small">Mama Mentors Terms of Use Agreement</h2>
                <p class="paragraph">
                    Mama Mentors is an initiative of Mama Mentors LLC, a limited liability corporation, ("Mama Mentors"). Mama Mentors’s mission is to match a woman in any stage of her journey through motherhood (conception, pregnancy, birth and beyond) with a more experienced mom counterpart for one-on-one support.
                </p>
                <p class="paragraph">
                    By accessing or using the Mama Mentors website, located at www.Mama Mentors.com, or any successor websites, including any and all international or mobile versions thereof (the "Website") or by otherwise accessing any portion of Mama Mentors's programs you ("you") agree to be bound by this Terms of Use Agreement (this "Agreement") and any agreements, policies and other documents incorporated by reference herein, whether or not you become a mentor, mentee, or otherwise participate ("User" or "Users") in Mama Mentors's programs. In addition, if you wish to become a registered User of the Website, please read this Terms of Use Agreement and indicate your acceptance by clicking the <span class="text-font-bold text-uppercase">JOIN MAMA MENTORS</span> button on Mama Mentors's Online Application Form.
                </p>
                <p class="paragraph">
                    We reserve the right, at our sole discretion, to change, modify, add, or delete portions of this Terms of Use at any time without further notice. If we do this, we will post the changes to this Terms of Use on this page and will indicate at the top of this page the date these terms were last revised. Your continued use of the Website after any such change constitutes your acceptance of the new Terms of Use. If you do not agree to this or any future Terms of Use, you may not use or access (or continue to use or access) the Website. It is your responsibility to regularly check the Website to determine if there have been changes to this Terms of Use and to review such changes.
                </p>
                <p class="paragraph">
                    You may receive a written copy of this Agreement by emailing us at: <a href="mailto:support@mamamentors.com" class="text-color-pink">support@mamamentors.com</a>, Subject: Mama Mentors Terms of Use Agreement.<br/><br/>
                </p>
                <div class="columns">
                    <ol type="1">
                        <li class="item">
                            <p>Term.</p>
                            <p>This Agreement will remain in full force and effect while you use the Website. Whether or not you qualify to be a mentor or mentee and are eligible to participate in the mentor/mentee program shall be determined by Mama Mentors in its sole and absolute discretion. If you are eligible to participate in the mentor/mentee program, you may terminate your participation for any reason upon receipt by Mama Mentors of your written or email notice of termination. Mama Mentors may also terminate your participation in the mentor/mentee program for any reason and at any time, effective upon sending notice to you at the email address you provide in your application for membership, or such other email address as you may later provide to Mama Mentors. Even after your participation in the mentor/mentee program is terminated or your use of the Website ceases, this Agreement will remain in effect. This Website is intended solely for users who are eighteen (18) years of age or older. Any registration by, use of or access to the Website by anyone under eighteen (18) is unauthorized, unlicensed and in violation of these Terms of Use. To the extent we decide in our sole and absolute discretion to charge for any of our services, we will notify you of same, and you agree to pay the appropriate fees in order to obtain such services.</p>
                        </li>
                        <li class="item">
                            <p>Registration Data; Account Security.</p>
                            <p>You agree to (a) provide accurate, current and complete information on any registration forms on the Website, including any mentor profile or mentee profile submitted by you ("Registration Data"); (b) maintain the security of your password and identification; (c) maintain and promptly update the Registration Data, and any other information you provide to us, to keep it accurate, current and complete; and (d) be fully responsible for all use of your account and for any actions that take place using your account. You hereby authorize Mama Mentors to verify the information supplied in your Mama Mentors Application when and as Mama Mentors deems necessary and advisable, and to the extent permitted by applicable law. Notwithstanding the foregoing, Mama Mentors shall be under no obligation to verify the information supplied in your Mama Mentors Application and may rely on such information without taking any further action. You consent to receiving communications and notices from Mama Mentors at the email address you provide in your Registration Data or otherwise elect in your account settings.</p>
                        </li>
                        <li class="item">
                            <p>Non Commercial Use.</p>
                            <p>The Website is for the use by Users in their individual capacities as Mama Mentors Users only and may not be used in connection with any commercial or other endeavors. Organizations, companies, and/or businesses may not use the Website for any purpose other than those contemplated in this Terms of Use or Privacy Policy. Illegal and/or unauthorized uses by you of the Website, including collecting user names and/or email addresses of Users by electronic or other means for any reason, including, but not limited to, the purpose of sending unsolicited emails and unauthorized framing of or linking to the Website is a violation of this Agreement, and appropriate legal action may be taken, including, without limitation, civil, criminal, and injunctive redress.</p>
                        </li>
                        <li class="item">
                            <p>Proprietary Rights in Website Content; Limited License.</p>
                            <p>All content on the Website and available through the mentor/mentee program, and their selection and arrangement, but excluding User Content (the "Website Content"), are the proprietary property of Mama Mentors, its users or its licensors, with all rights reserved. No Website Content may be modified, copied, distributed, framed, reproduced, republished, downloaded, scraped, displayed, posted, transmitted, or sold in any form or by any means, in whole or in part, without our prior written permission. Subject to these Terms of Use, you are granted a limited license to access and use the Website and the Website Content and to download or print a copy of any properly-accessed portion of the Website Content solely for your personal, non-commercial use, provided that you keep all copyright or other proprietary notices intact. Such license does not permit use of any data mining, robots, scraping or similar data gathering or extraction methods. Unless explicitly stated herein, nothing in these Terms of Use will be construed as conferring any license to intellectual property rights, whether by estoppel, implication or otherwise. We may revoke this license at any time without notice and with or without cause.</p>
                        </li>
                        <li class="item">
                            <p>Trademarks.</p>
                            <p>"Mama Mentors", the "Mama Mentors" logo and other Mama Mentors graphics, logos, designs, page headers, button icons, scripts and service names are our trademarks or trade dress in the U.S. and/or other countries. Our trademarks and trade dress may not be used, including as part of trademarks and/or as part of domain names, in connection with any product or service in any manner that is likely to cause confusion and may not be copied, imitated, or used, in whole or in part, without our prior written permission.</p>
                        </li>
                        <li class="item">
                            <p>User Content Posted on the Website.</p>
                            <p>
                                <ol type="a">
                                    <li>
                                        <p>You understand and agree that Mama Mentors may (but is not obligated to) review and delete (without notice) any content, messages, or profiles provided by you (collectively, "User Content") that in the sole judgment of Mama Mentors violate this Agreement or which might be offensive, illegal, or that might violate the rights, harm, or threaten the safety of others. You are solely responsible at your sole cost and expense for creating backup copies and replacing any User Content you post on the Website or provide to us.</p>
                                    </li>
                                    <li>
                                        <p>By posting User Content to the Website, you hereby grant, and you represent and warrant that you have the right, power and authority to grant to Mama Mentors an irrevocable, perpetual, non-exclusive, fully paid, worldwide license to use, copy, perform, display, distribute, reformat, translate and excerpt such User Content and to prepare derivative works of, or incorporate into other works, such User Content for any purpose, and to grant and authorize sublicenses of the foregoing. If a User contacts Mama Mentors to deactivate their account, their profile and mentoring requests will no longer be available on the Website. However, some information may remain on the Website after a User deactivates their profile, including, but not limited to, a history of messages, mentoring offers and matches associated with that User on the mentoring activity history of other Users.</p>
                                    </li>
                                    <li>
                                        <p>You agree to the following:</p>
                                        <p>
                                            <ol type="1">
                                                <li>You will not send or otherwise post unauthorized commercial communications to Users (such as spam).</li>
                                                <li>You will not collect Users' information, or otherwise access the Website, using automated means (such as harvesting bots, robots, spiders, or scrapers) without our permission.</li>
                                                <li>You will not upload viruses or other malicious code.</li>
                                                <li>You will not solicit login information or access an account belonging to someone else.</li>
                                                <li>You will not bully, intimidate, or harass any User.</li>
                                                <li>You will not post content that is hateful, threatening, pornographic, that contains nudity or graphic or gratuitous violence, or that violates applicable law.</li>
                                                <li>You will not promote alcohol-related or other mature content without appropriate age-based restrictions.</li>
                                                <li>You will not use the Website to do anything unlawful, misleading, malicious, or discriminatory.</li>
                                                <li>You will not facilitate or encourage any violations of this Term of Use.</li>
                                                <li>If you collect information from users, you will: obtain their consent, make it clear you (and not Mama Mentors) are the one collecting their information, and post your privacy policy.</li>
                                                <li>You will not post anyone's social security, driver's license, credit card, debit card, or bank account numbers or other similar content on the Website.</li>
                                                <li>You will not use Mama Mentors if you are located in a country embargoed by the United States, or are on the United States Treasury Department's list of Specially Designated Nationals.</li>
                                            </ol>
                                        </p>
                                    </li>
                                    <li>
                                        In order to protect Users from unauthorized advertising or solicitation, Mama Mentors reserves the right to restrict the number of emails or other communications which a User may send to other Users in any 24-hour or other period to a number which Mama Mentors deems appropriate, in Mama Mentors's sole and absolute discretion.
                                    </li>
                                </ol>
                            </p>
                        </li>
                        <li class="item">
                            <p>Privacy.</p>
                            <p>Use of the Website and/or the mentor/mentee program is also governed by our Privacy Policy and Code of Conduct.</p>
                        </li>
                        <li class="item">
                            <p>Intellectual Property Rights Complaints.</p>
                            <p>When we receive proper notification of alleged copyright infringement, we will investigate and may remove or disable access to the allegedly infringing material and may terminate, in our sole discretion, the accounts of repeat infringers in accordance with the Digital Millennium Copyright Act and other applicable law. We may also, at our sole discretion, limit access to the Website and/or terminate the memberships of any Users who infringe any intellectual property rights of others, whether or not there is any repeat infringement. If you believe that any material on the Website infringes upon any copyright which you own or control, you may flag that content as inappropriate. Mama Mentors will assess the material and may remove the material from public viewing.</p>
                        </li>
                        <li class="item">
                            <p>Third Party Websites and Content.</p>
                            <p>The Website contains (or you may be sent through the Website) links to other web sites ("Third Party Websites") as well as articles, photographs, text, items and other content belonging to or originating from third parties (the "Third Party Components"). Such Third Party Websites and Third Party Components are not investigated, monitored or checked for accuracy, appropriateness, or completeness by us, and we are not responsible for the acts or omissions of any Third Party Websites or Third Party Components. If you decide to leave the Website and access the Third Party Websites or to use or install any Third Party Components, you do so at your own risk and our terms and policies no longer govern. You should review the applicable terms and policies, including privacy and data gathering practices, of any Third Party Website to which you navigate from the Website or relating to any Third Party Components.</p>
                        </li>
                        <li class="item">
                            <p>Content Feeds and Viral Distribution Widgets.</p>
                            <p>In order to promote the volunteer opportunities available on the Website, some features of your profile may be exported to Third Party Sites to notify prospective users of the kinds of volunteer opportunities that exist on the Website. Such features that may be exported to Third Party Sites include, but are not limited to, your name, picture, industry, business name, experience needed, city and state of business, mentoring request summary, mentoring request expertise, profile URL and mentoring request URL.</p>
                        </li>
                        <li class="item">
                            <p>User Messaging.</p>
                            <p>By sending communications to other Users (including, but not limited to, mentor/mentee requests), you consent to Mama Mentors storing the conversations you have with other Users on its servers owned or controlled by us. You understand that even if you delete a conversation you have with another User, that User may also be able to save it. You further agree that we may retain, use, and scan copies of your conversations for a limited time solely to filter spam, detect viruses, and test and improve our messaging services. We may also scan the text of your conversations in order to filter spam and detect viruses.</p>
                        </li>
                        <li class="item">
                            <p>Disclaimer of Warranties.</p>
                            <p>
                                <ol type="a">
                                    <li>YOUR USE OF THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE, ARE AT YOUR SOLE RISK. THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE, ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. MAMA MENTORS AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, LICENSORS AND USERS EXPRESSLY DISCLAIM TO YOU ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. FOR PURPOSES OF CLARITY, YOU UNDERSTAND AND AGREE THAT ALL DECISIONS MADE BY YOU WITH RESPECT TO AND AS A RESULT OF PARTICIPATING IN THE PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE ARE YOUR'S ALONE, AND MAMA MENTORS DOES NOT MONITOR OR VERIFY THE ACCURACY OR RELIABILITY OF ANY INFORMATION OR ADVICE PROVIDED ON OR VIA THE WEBSITE AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE.</li>
                                    <li>MAMA MENTORS AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, LICENSORS AND USERS MAKE NO WARRANTY TO YOU THAT (i) THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE, WILL MEET YOUR REQUIREMENTS; (ii) THE WEBSITE WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE; (iii) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE, WILL BE ACCURATE, RELIABLE OR GENERATE BENEFIT (INCLUDING WITHOUT LIMITATION ECONOMIC BENEFIT) TO YOU; AND (iv) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE WEBSITE.</li>
                                    <li>ANY MATERIAL OR INFORMATION DOWNLOADED, OTHERWISE OBTAINED OR USED THROUGH THE USE OF THE WEBSITE OR ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE IS MADE AVAILABLE AT YOUR OWN RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR AND HEREBY WAIVE ANY AND ALL CLAIMS AND CAUSES OF ACTION WITH RESPECT TO ANY DAMAGE TO YOUR COMPUTER SYSTEM, INTERNET ACCESS, BUSINESS, BUSINESS REPUTATION, DOWNLOAD OR DISPLAY DEVICE, OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OR USE OF ANY SUCH MATERIAL OR INFORMATION.</li>
                                    <li>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM MAMA MENTORS OR THROUGH OR FROM THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE, SHALL CREATE ANY WARRANTY AND ARE PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</li>
                                </ol>
                            </p>
                        </li>
                        <li class="item">
                            <p>Limitation on Liability.</p>
                            <p>YOU EXPRESSLY UNDERSTAND AND AGREE THAT MAMA MENTORS AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS, LICENSORS AND USERS SHALL NOT BE LIABLE TO YOU FOR ANY DIRECT, PUNITIVE, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, COSTS, LOSSES OR EXPENSES, INCLUDING, BUT NOT LIMITED TO, THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF MAMA MENTORS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM: (a) THE USE OR THE INABILITY TO USE THE WEBSITE OR ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE ; (b) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (c) STATEMENTS, ADVICE, PARTICIPATING IN MENTORING RELATIONSHIPS OR CONDUCT OF ANY THIRD PARTY ON THE WEBSITE; OR (d) ANY OTHER MATTER RELATING TO THE WEBSITE, AND ANY PROGRAMS OR SERVICES OFFERED VIA THE WEBSITE.</p>
                        </li>
                        <li class="item">
                            <p>Exclusions and Limitations.</p>
                            <p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS OF SECTIONS 12 AND 13 MAY NOT APPLY TO YOU TO THE EXTENT SO PROHIBITED BY LAW.</p>
                        </li>
                        <li class="item">
                            <p>Disputes with other Users.</p>
                            <p>You are solely responsible for your interactions with Users and any disputes that may result from such interactions. Mama Mentors, Inc. reserves the right, but has no obligation, to monitor disputes between you and other Users.</p>
                        </li>
                        <li class="item">
                            <p>Disputes with Mama Mentors.</p>
                            <p>If there is any dispute about or involving the Website, you agree that the dispute will be governed by the laws of the State of California without regard to its conflict of law provisions and will be resolved in a state or federal court located in Los Angeles, California. You agree to personal jurisdiction by and venue in the state and federal courts of the State of California, City of Los Angeles.</p>
                        </li>
                        <li class="item">
                            <p>Indemnity.</p>
                            <p>You agree to indemnify and hold Mama Mentors, its subsidiaries and affiliates, and each of their directors, officers, agents, contractors, partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorney's fees, made by any third party due to or arising out of your use of the Website, any User Content you post on or through the Website, your conduct in connection with your participation in Mama Mentors's programs and/or arising from a breach of this Agreement.</p>
                        </li>
                        <li class="item">
                            <p>Other.</p>
                            <p>This Agreement (together with any agreements, policies or other documents incorporated by reference herein), accepted upon use of the Website and further affirmed by becoming a registered User, contains the entire agreement between you and Mama Mentors regarding the use of the Website and/or the programs offered by Mama Mentors. If any provision of this Agreement is held invalid, the remainder of this Agreement shall continue in full force and effect. Our failure to exercise or enforce any right or provision of these Terms of Use will not constitute a waiver of such right or provision in that or any other instance. If any provision of these Terms of Use is held invalid, the remainder of these Terms of Use will continue in full force and effect. If any provision of these Terms of Use is deemed unlawful, void or for any reason unenforceable, then that provision will be deemed severable from these Terms of Use and will not affect the validity and enforceability of any remaining provisions. These Terms of Use are in the English language only, which language will be controlling in all respects, and all versions hereof in any other language will not be binding on the parties hereto. All communications and notices to be made or given pursuant to these Terms of Use will be in the English language.</p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
	
@endsection