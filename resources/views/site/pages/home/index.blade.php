@extends('site.layout.default')

@section('title', 'Home Title')
@section('description', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.')
@section('id', 'home')
@section('content')

	<section class="highlight">
		<div class="column row">
			<div class="small-12 medium-6 columns">
				<div class="highlight-infos">
					<h1 class="title space-out-bottom-default">Reimagine Motherhood <br class="hide-for-small-only" />with a <span class="text-font-bold text-color-pink">Mentor</span></h1> 
					<p class="text-paragraph space-out-bottom-default">1-on-1 support for real advice from a real mom <br class="hide-for-small-only" />about real experiences.<p>
					<a class="button-apple">
						<img src="assets/site/images/logo-apple@1x.png" alt="Coming soon to Apple Store"
							srcset="assets/site/images/logo-apple@2x.png, assets/site/images/logo-apple@3x.png"
							width="130" height="39">
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="community space-in-default diagonal-top diagonal-bottom">
		<div class="column row">
			<p class="help-text text-center text-uppercase text-font-black text-color-blue">Support Mama Mentors</p>
			<h2 class="text-center section-title space-out-bottom-small">Join Our Community</h2>
			<p class="text-center text-paragraph">
				Receive updates on the community & an early invitation when we launch our beta.
			</p>
		</div>
		<div class="column row">
			<form action="#" class="form-community" id="form-community">
				<div class="small-12 medium-6 large-offset-1 large-5 columns">
					<div class="form-group">
						<input class="input-field space-out-bottom-small" type="text" placeholder="enter email address">	
					</div>
				</div>
				<div class="small-12 medium-6 large-pull-1 large-5 columns">
					<div class="form-group">
						<button class="button-solid" type="submit">sign up and get updates</button>	
					</div>
				</div>	
			</form>
		</div>
	</section>

	<section class="mamamentors space-in-top-big">
		<div class="row expanded small-collapse medium-collapse large-collapse">
			<div class="small-12 medium-6 columns">
				<div class="mamamentors-infos-container diagonal-bottom">
					<div class="mamamentors-infos">
						<h2 class="section-title text-color-white space-out-bottom-small"><span class="text-font-bold">What</span> is Mama Mentors?</h2>
						<p class="text-paragraph text-color-white space-out-bottom-big">
							Mama Mentors is the first platform connecting a woman in any part of her journey through motherhood (conception, pregnancy, birth and beyond) with a more experienced mom counterpart.
						</p>
					</div>
				</div>
			</div>
			<div class="small-12 medium-6 columns">
				<a href="#" class="video-mamamentors">
				</a>
			</div>
		</div>
	</section>

	<section class="real-sharing space-in-bigger diagonal-top clipping">
		<div class="column row rotate-back">
			<div class="small-12 medium-5 medium-offset-6 columns">
				<h2 class="section-title space-out-bottom-small"><span class="text-font-bold text-color-pink">Real Sharing</span> & Understanding</h2>
				<p class="text-paragraph space-out-bottom-default">
					Mama Mentors allows women to carefully select their mentor or mentee based on shared life experiences, creating an intimate space to confidently ask questions and find a common bond on becoming or being a mom.
				</p>

				<h3 class="help-text text-color-blue text-font-black text-uppercase space-out-bottom-small">
					<span class="text-bold">What are we talking about?</span>
				</h3>
				<p class="text-paragraph">
					Infertility, postpartum depression, older motherhood, breastfeeding, raising multiples, divorce, adoption, raising special needs children, multiracial families.
				</p>
			</div>
		</div>
	</section>

	<section class="how-it-works space-in-bigger diagonal-top diagonal-bottom">
		<div class="column row">
			<h2 class="text-center section-title space-out-bottom-bigger"><span class="text-font-bold text-color-pink">How</span> it works</h2>
		</div>
		<div class="column row">
			<ul class="list tutorial-list">
				<li class="tutorial-item small-12 medium-6 large-3 columns">
					<h3 class="text-center subtitle text-font-black text-color-pink text-uppercase">Filter</h3>
					<p class="text-center text-paragraph space-out-bottom-big">
						Our filtering system allows you to find the right mom for your unique situation.
					</p>
					<img class="device-screen float-center" src="assets/site/images/how-it-works-1@1x.png" alt="How it works - Filter"
						srcset="assets/site/images/how-it-works-1@2x.png, assets/site/images/how-it-works-1@3x.png"
						width="292" height="auto">
				</li>
				<li class="tutorial-item small-12 medium-6 large-3 columns">
					<h3 class="text-center subtitle text-font-black text-color-pink text-uppercase">Discover</h3>
					<p class="text-center text-paragraph space-out-bottom-big">
						Read through profiles from real women who want to share their knowledge.
					</p>
					<img class="device-screen float-center" src="assets/site/images/how-it-works-2@1x.png" alt="How it works - Filter"
						srcset="assets/site/images/how-it-works-2@2x.png, assets/site/images/how-it-works-2@3x.png"
						width="292" height="auto">
				</li>
				<li class="tutorial-item small-12 medium-6 large-3 columns">
					<h3 class="text-center subtitle text-font-black text-color-pink text-uppercase">Request</h3>
					<p class="text-center text-paragraph space-out-bottom-big">
						No more awkward Friend Requests, we will introduce you to your mentor like a friend would.
					</p>
					<img class="device-screen float-center" src="assets/site/images/how-it-works-3@1x.png" alt="How it works - Filter"
						srcset="assets/site/images/how-it-works-3@2x.png, assets/site/images/how-it-works-3@3x.png"
						width="292" height="auto">
				</li>
				<li class="tutorial-item small-12 medium-6 large-3 columns">
					<h3 class="text-center subtitle text-font-black text-color-pink text-uppercase">Share</h3>
					<p class="text-center text-paragraph space-out-bottom-big">
						Ask questions or simply unload - our private messaging tool means what is shared is only between you and your mentor.
					</p>
					<img class="device-screen float-center" src="assets/site/images/how-it-works-4@1x.png" alt="How it works - Filter"
						srcset="assets/site/images/how-it-works-4@2x.png, assets/site/images/how-it-works-4@3x.png"
						width="292" height="auto">
				</li>
			</ul>
		</div>
	</section>

	<section class="support space-in-bigger">
		<div class="column row">
			<div class="small-12 columns">
				<img src="assets/site/images/icon-private-message-white.png" alt="Icon private message"
					width="41" height="auto" class="float-center">
				<h2 class="section-title text-color-white text-center space-out-top-small space-out-bottom-big">I <span class="text-font-bold">support</span> Mama Mentors because</h2>
			</div>
		</div>
		<div class="row expanded">
			<div class="list-testmonial">
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-1@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-1@2x.jpg, assets/site/images/testmonial-1@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								Being a mom is the hardest, most rewarding thing I've ever done. There are daily challenges and rewards that only another mama can truly appreciate. Having someone to share frustrations and victories with (no matter how small) and turn to for advice is so important.
								<span class="author">
									Ashley F., Portland, OR
								</span>
							</p>
						</div>
					</div>
				</a>
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-2@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-2@2x.jpg, assets/site/images/testmonial-2@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								It takes a village...sometimes you need that village to give you advice and direction and sometimes you just need the village to say "you are doing the best you can and there is nothing more important than that!" And maybe bring you a glass of wine from time to time.
								<span class="author">
									Whitney T., Boston, Massachusetts
								</span>
							</p>
						</div>
					</div>
				</a>
				<a href="#" class="testmonial-item">
					<div class="float-center list-testmonial-item"> 	
						<img class="testmonial-image" src="assets/site/images/testmonial-1@1x.jpg" alt="Asheley Testmonial"
							srcset="assets/site/images/testmonial-1@2x.jpg, assets/site/images/testmonial-1@3x.jpg"
							width="302" height="158" />
						<div class="testmonial-information text-center">
							<p class="text-paragraph">
								Being a mom is the hardest, most rewarding thing I've ever done. There are daily challenges and rewards that only another mama can truly appreciate. Having someone to share frustrations and victories with (no matter how small) and turn to for advice is so important.
								<span class="author">
									Ashley F., Portland, OR
								</span>
							</p>
						</div>
					</div>
				</a>
			</ul>
		</div>
		<div class="column row show-for-small-only">
			<ul class="list testmonials-pagination">
				<li class="page-item">
					<a href="#" class="page-link">Page</a>
				</li>
				<li class="page-item">
					<a href="#" class="page-link active">Page</a>
				</li>
				<li class="page-item">
					<a href="#" class="page-link">Page</a>
				</li>
			</ul>
		</div>
	</section>

	<section class="why-join space-in-big diagonal-top diagonal-bottom">
		<div class="column row">
			<div class="small-12 columns">
				<h2 class="section-title space-out-bottom-bigger"><span class="text-font-bold text-color-pink">Why</span> You Should Join</h2>
			</div>
		</div>
		<div class="column row">
			<ul class="list list-reasons">
				<li class="list-reason-item small-12 medium-6 columns">
					<h3 class="subtitle space-out-bottom-smaller text-color-blue">Every Mom is a Mentor</h3>
					<p class="text-paragraph">Any woman who’s experienced the ups and downs of motherhood is a mentor.</p>
				</li>
				<li class="list-reason-item small-12 medium-6 columns">
					<h3 class="subtitle space-out-bottom-smaller text-color-blue">Guided Mentorship</h3>
					<p class="text-paragraph">You will receive the tools to foster a meaningful relationship between you and your mentor or mentee.</p>
				</li>
				<li class="list-reason-item small-12 medium-6 columns">
					<h3 class="subtitle space-out-bottom-smaller text-color-blue">Safe & Private</h3>
					<p class="text-paragraph">All members are verified and you choose what information to share in your public profile.</p>
				</li>
				<li class="list-reason-item small-12 medium-6 columns">
					<h3 class="subtitle space-out-bottom-smaller text-color-blue">Help a Mother in Need</h3>
					<p class="text-paragraph">With just five minutes out of your day, you can positively impact another woman’s journey.</p>
				</li>
			</ul>
		</div>
	</section>

	<section class="quote space-in-bigger">
		<div class="column row">
			<div class="small-12 columns">
				<div class="brand logo-vertical float-center">
					<?php echo file_get_contents("assets/site/images/logo-mamamentors-vertical.svg"); ?>
				</div>
			</div>
			<div class="small-12 columns">
				<h2 class="section-title text-color-grey text-center"><span class="text-font-bold text-color-pink">Share</span> the drama with your Mama.</h2>
			</div>
		</div>
	</section>

	<section class="subscribe space-in-big diagonal-top diagonal-bottom">
		<div class="column row">
			<div class="small-12 columns">
				<h3 class="help-text text-center text-font-black text-color-blue text-uppercase space-out-bottom-smaller">Get Involved</h3>
				<h2 class="section-title text-center space-out-bottom-small">Subscribe to Our Newsletter</h2>
				<p class="text-paragraph text-center space-out-bottom-default">
					Sign up for updates and receive an early invitation when we launch our beta.  As mentor, mentee or both – we can empower every woman to experience motherhood as a team.
				</p>
			</div>
		</div>
		<div class="column row">
			<form action="#">
				<div class="small-12 medium-6 large-offset-1 large-5 columns">
					<div class="form-group">
						<input class="input-field space-out-bottom-small" type="text" placeholder="enter email address">	
					</div>
				</div>
				<div class="small-12 medium-6 large-pull-1 large-5 columns">
					<div class="form-group">
						<button class="button-solid" type="submit">sign up and get updates</button>	
					</div>
				</div>	
			</form>
		</div>
	</section>
	
@endsection