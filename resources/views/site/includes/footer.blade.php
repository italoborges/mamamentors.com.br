<footer class="footer text-center">
    <div class="column row">
        <a class="brand logo-vertical float-center space-out-bottom-small">
            <?php echo file_get_contents("assets/site/images/logo-mamamentors-vertical.svg"); ?>
        </a>
        <p class="text-color-white space-bottom-default">
            <span class="text-font-black space-out-bottom-small">
                We want to hear from you!
            </span><br/> 
            Your ongoing support to help us build this community means the world to us. Say hello at
            <a class="mail text-color-yellow" href="mailto:heymama@mamamentors.com">heymama@mamamentors.com</a>
        </p>
    </div>

    <div class="column row">
        <p class="text-color-white text-font-black">
            Join Us On
        </p>
        <ul class="social-media">
            <li class="social-item">
                <a class="social-link" href="#">Twitter</a>
            </li>
            <li class="social-item">
                <a class="social-link" href="#">Facebook</a>
            </li>
            <li class="social-item">
                <a class="social-link" href="#">Instagram</a>
            </li>
        </ul>
    </div>

    <div class="column row space-out-bottom-big">
        <ul class="list nav-menu">
            <li class="nav-menu-item">
                <a class="nav-menu-link" href="#">About Us</a>
            </li>
            <li class="nav-menu-item">
                <a class="nav-menu-link" href="#">Blog</a>
            </li>
            <li class="nav-menu-item">
                <a class="nav-menu-link" href="#">Timeline</a>
            </li>
            <li class="nav-menu-item">
                <a class="nav-menu-link" href="#">Terms of Use</a>
            </li>
            <li class="nav-menu-item">
                <a class="nav-menu-link" href="#">Privacy Policy</a>
            </li>
        </ul>
    </div>

    <div class="column row small-collapse medium-uncollapse">
        <div class="small-8 columns">
            <p class="reserved-info text-left">
                © 2016 Mama Mentors - All Rights Reserved<br/>
                Website Design - Kollettiv
            </p>
        </div>
        <div class="small-4 float-right columns">
            <a class="button-apple">
                <img src="assets/site/images/logo-apple@1x.png" alt="Coming soon to Apple Store"
                    srcset="assets/site/images/logo-apple@2x.png, assets/site/images/logo-apple@3x.png"
                    width="130" height="39">
            </a>
        </div>
    </div>
</footer>