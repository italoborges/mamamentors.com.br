<header class="header">
    <div class="expanded row">
        <div class="small-12 columns">
            <a href="#" class="brand logo-horizontal float-left">
                <?php echo file_get_contents("assets/site/images/logo-mamamentors-horizontal.svg"); ?>
            </a>
            <div class="nav-buttons float-right">
                <nav class="menu hide-for-small-only">
                    <ul class="menu-buttons">
                        <li class="menu-button">
                            <a href="#" class="menu-link">Home</a>
                        </li>
                        <li class="menu-button">
                            <a href="#" class="menu-link">About</a>
                        </li>
                        <li class="menu-button">
                            <a href="#" class="menu-link">Timeline</a>
                        </li>
                        <li class="menu-button">
                            <a href="#" class="menu-link">Blog</a>
                        </li>
                    </ul>
                </nav>
                <a href="#" class="button-underline">Sign Up</a>
                <a href="#" class="button-nav-menu show-for-small-only">
                    <svg width="20" height="14" class="button-nav-icon" viewBox="0 0 20 14">
                        <line class="menu-line" x1="2" y1="1" x2="18" y2="1" />
                        <line class="menu-line" x1="2" y1="7" x2="18" y2="7" />
                        <line class="menu-line" x1="2" y1="13" x2="18" y2="13" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
</header>