<nav class="menu-mobile show-only-for-small">
    <ul class="nav-menu">
        <li class="nav-item">
            <a href="#" class="nav-button">Home</a>	
        </li>
        <li class="nav-item">
            <a href="#" class="nav-button">About Us</a>	
        </li>
        <li class="nav-item">
            <a href="#" class="nav-button">Timeline</a>	
        </li>
        <li class="nav-item">
            <a href="#" class="nav-button">Blog</a>	
        </li>
    </ul>
    <ul class="nav-links">
        <li class="nav-item">
            <a href="#" class="nav-button nav-button-sm">Terms of Use</a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-button nav-button-sm">Privacy Policy</a>
        </li>
    </ul>
    <a href="#" class="button-solid">sign up and get updates</a>
</nav>