<?php 
	header("Content-Type: text/html; charset=utf-8"); 
	header("Connection: keep-alive");
?>
<!DOCTYPE html>
<html lang="en" xml:lang="en-US">
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
		<meta name="description" content="@yield('description')" >

        <link rel="canonical" href="{{ Request::url() }}">
		<link rel="alternate" hreflang="en-US" href="{{ Request::url() }}">

		<link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,400i,700|Source+Sans+Pro:400,700,900" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{!! App::environment() == 'local' ? asset('assets/site/css/main.css') : asset_version('assets/site/css/main.min.css') !!}">
	</head>
    <body>
        <h1 class="visibility-hidden">@yield('title')</h1>

		@include('site.includes.nav-mobile')

		<div class="container-app">
			@include('site.includes.header')

			<main class="content" id="@yield('id')">
				@yield ('content')
			</main>

			@include('site.includes.footer')
		</div>

		<script src="{!! App::environment() == 'local' ? asset('assets/site/js/main.js') : asset_version('assets/site/js/main.min.js') !!}" async></script>
		<script type="text/javascript">
			
		</script>
    </body>
</html>