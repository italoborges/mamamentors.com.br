(function ($) {

    $(window).setBreakpoints({
        distinct: true, 
        breakpoints: [
            320,
            640,
            1024
        ] 
    });

    /**
     * MENU MOBILE
     */
    $('.button-nav-menu').on('click', function (e) {
        e.preventDefault();

        if(!$('body').hasClass('menu-mobile-in'))
            openMenu();
        else
            closeMenu();

        return false;
    });

    function openMenu () {
        $('body').addClass('menu-mobile-in');
        $('.container-app').on('click', function (e) {
            closeMenu();
        });
    }

    function closeMenu () {
        $('body').removeClass('menu-mobile-in');
        $('.container-app').unbind('click');
    }

    /**
     * SUPPORT
     */

    var testmonials = {};
    if($('#home').length > 0) {
        testmonials.items = [];
        testmonials.current = 1;
        testmonials.container = $('.list-testmonial');
        testmonials.pagination = $('.testmonials-pagination .page-link');

        $('.testmonial-item').each(function (index, element) {
            testmonials.items.push(element);

            $(element).on('click', function (e) {
                e.preventDefault();
                testmonials.current = index;
                arrangeTestmonials();
                return false;
            });
        });

        $('.testmonials-pagination .page-link').each(function (index, element) {
            $(element).on('click', function (e) {
                e.preventDefault();
                testmonials.current = index;
                arrangeTestmonials();
                return false;
            });
        });

        testmonials.container.on('swipeleft', function () {
            testmonials.current++;
            arrangeTestmonials();
        });

        testmonials.container.on('swiperight', function () {
            testmonials.current--;
            arrangeTestmonials();
        });

        $(window).bind('enterBreakpoint320',function() {
            testmonials.current = 1;
            arrangeTestmonials();
        });

        $(window).bind('exitBreakpoint320',function() {
            for(var i = 0; i < testmonials.items.length; i++) {
                console.log(i);
                $(testmonials.items[i]).css({
                    'left' : '',
                    'transform' : ''
                });
            }
        });
    }

    function updatePagination () {
        testmonials.pagination.removeClass('active');
        testmonials.pagination.eq(testmonials.current).addClass('active');
    }

    function arrangeTestmonials () {
        if(testmonials.current < 0) {
            testmonials.current = 0;
        }

        if(testmonials.current > testmonials.items.length - 1) {
            testmonials.current = testmonials.items.length - 1;
        }

        var i = 0;
        for(i = 0; i < testmonials.current; i++) {
            $(testmonials.items[i]).css({
                'left' : -((testmonials.current - i) * 303) + 'px',
                'transform' : 'translateX(-50%) scale(0.9)'
            });
        }

        $(testmonials.items[testmonials.current]).css({
            'left' : '0',
            'transform' : 'translateX(-50%) scale(1)'
        });

        for(i = testmonials.current + 1; i < testmonials.items.length; i++) {
            $(testmonials.items[i]).css({
                'left' : ( (i - testmonials.current) * 303) + 'px',
                'transform' : 'translateX(-50%) scale(0.9)'
            });
        }

        updatePagination();
    }

})(jQuery);